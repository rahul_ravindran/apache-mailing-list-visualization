import logging
from twisted.internet import reactor
from scrapy.crawler import CrawlerProcess
from scrapy.selector import HtmlXPathSelector
from scrapy.item import Item,Field
from scrapy.http import Request
import scrapy

class ApacheSpider(scrapy.Spider):
    name = 'apache_archive'
    allowed_domains = ['mail-archives.apache.org']
    start_urls = []
    logger = logging.getLogger()
    def __init__(self):
        for i in range(2013,2016):
            for j in range(1,13):
                self.start_urls.append('https://mail-archives.apache.org/mod_mbox/kafka-commits/' \
                    + str(i) + str(str(j).zfill(2)) + '.mbox/browser')
    def parse(self,response):
        for url in response.xpath('//td/a/@href').extract():
            print url.strip('ajax/%')[:-2]
            yield Request("response.url +'/'+ '<'+url.strip('ajax/%')[:-2] + '>'",callback = self.scrapEmail)

    def scrapEmail(self,response):
        # temphxs = HtmlXPathSelector(response)
        emaildata = response.xpath('//pre/text()').extract()
        return emaildata

# class DataSchema(Item):

if __name__ == '__main__':
    runner = CrawlerProcess()
    runner.crawl(ApacheSpider)
    runner.start()
